
const path = require('path');
const env = process.env.VUE_APP_BUILD;
let proxyObj = {};
if (env === 'devtest') {
  proxyObj = {
    '/': {
      changeOrigin: true,
      target: 'http://139.224.114.61:80',
      pathRewrite: {
        '^/api/v1': ''
      }
    },
  }
} else if (env === 'devall') {
  proxyObj = {
    '/': {
      changeOrigin: true,
      // target: 'http://localhost:90',
      target:"http://47.93.212.63",
      pathRewrite: {
        '^/api/v1': ''
      }
    },
  }
} else {
  proxyObj = {
    '/': {
      changeOrigin: true,
      target: 'http://127.0.0.1:6001',
      pathRewrite: {
        '^/api/v1': ''
      }
    },
  }
}
module.exports = {
  productionSourceMap: false,
  chainWebpack: config => {
    const types = ['vue-modules', 'vue', 'normal-modules', 'normal'];
    types.forEach(type => addStyleResource(config.module.rule('less').oneOf(type)));
    //修改文件引入自定义路径
    config.resolve.alias
      .set('@$', resolve('src'))
      .set('@common', resolve('./src/application/common'))
      .set('@img', resolve('./src/assets/image'))
  },
  devServer: {
    open: true,
    host: '0.0.0.0',
    port: 8066,
    https: false,
    hotOnly: false,
    proxy: proxyObj,
  },
  // publicPath: process.env.NODE_ENV === 'production'? '/publicaccount/': '/'
  // publicPath: '/',
  publicPath: '/mlfast-demo',
  css: {
    loaderOptions: {
      postcss: {
        plugins: [require('tailwindcss'), require('autoprefixer')]
      },
      less: {
        javascriptEnabled: true
      }
    },
  },
};

//配置全局less文件
function addStyleResource(rule) {
  rule.use('style-resource')
    .loader('style-resources-loader')
    .options({
      patterns: [path.resolve(__dirname, "./src/assets/css/_theme1.less")]
    });
}

function resolve(dir) {
  return path.join(__dirname, dir)
}

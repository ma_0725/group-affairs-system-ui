const purgecss = require('@fullhuman/postcss-purgecss')({
  content: [
      './src/**/*.html',
      './src/**/*.vue',
      './src/**/*.jsx',
  ],

  // Include any special characters you're using in this regular expression
  defaultExtractor: content => content.match(/[\w-/:]+(?<!:)/g) || [],
  whitelist: [],
  whitelistPatterns: [ /-(leave|enter|appear)(|-(to|from|active))$/, /^(?!(|.*?:)cursor-move).+-move$/, /^router-link(|-exact)-active$/, /data-v-.*/ ],
})

module.exports = {
  plugins: [
      require('postcss-import'),
      require('tailwindcss'),
      require('postcss-preset-env')({ stage: 1 }),
      require('postcss-nested'),
      require('autoprefixer'),
      ...process.env.NODE_ENV === 'production'
          ? [purgecss]
          : []
  ]
}
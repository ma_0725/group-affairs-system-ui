import footer from './footer'
import header from './header'
import sider from './sider'
import breadcrum from './breadcrumb'
import G2plot from './g2plot'

const install = (Vue) => {
  Vue.component(footer.name, footer)
  Vue.component(header.name, header)
  Vue.component(sider.name, sider)
  Vue.component(breadcrum.name, breadcrum)
  Vue.component(G2plot.name, G2plot)
}

if (typeof window !== 'undefined' && window.Vue) {
  window.Vue.use(install)
}

export default {
  install
}

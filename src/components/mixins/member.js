export const member = {
  data() {
    return{
      memberList: [],
    }
  },
  mounted(){
    this.init();
  },
  methods: {
    getMemberName(val) {
      try {
        let memArr = this.memberList.filter(item => item.id === val)
        if(memArr && memArr.length){
          return memArr[0].name
        } else {
          return '--'
        }
      } catch(err) {
        return '--'
      }
    },
    init() {
      this.$ajax({
        url: this.$api.member.findList,
        data: {
          status: 2
        },
        success: (result) => {
          this.memberList = result;
        },
      });
    },
  },
}
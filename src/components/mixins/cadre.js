export const cadre = {
  data() {
    return{
      cadreList: [],
    }
  },
  mounted(){
    this.init();
  },
  methods: {
    getCadreName(val) {
      try {
        let cadArr = this.cadreList.filter(item => item.id === val)
        if(cadArr && cadArr.length){
          return cadArr[0].name
        } else {
          return '--'
        }
      } catch(err) {
        return ''
      }
    },
    init() {
      this.$ajax({
        url: this.$api.cadre.findList,
        data: {
          status: 2
        },
        success: (result) => {
          this.cadreList = result;
        },
      });
    },
  },
}
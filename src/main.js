import Vue from 'vue'
import App from './App.vue'
import router from './application/router'
import store from './application/vuex'
import MyComponents from './components'

//调试模式
Vue.config.productionTip = false;


import RouterTab from 'vue-router-tab'
import 'vue-router-tab/dist/lib/vue-router-tab.css'

//公用css
import './assets/css/common.css'
import './assets/css/tailwind.css'
import './assets/css/scrollbar.css'
import 'animate.css'
import './assets/css/ant.less'

import * as Antv from '@antv/g2plot'

// vue pc端ui框架 -- iview
import Antd from 'ant-design-vue'
import 'ant-design-vue/dist/antd.css';
import './assets/css/reset.less'

import moment from 'moment';
import 'moment/locale/zh-cn';
moment.locale('zh-cn');

import vChart from 'v-charts'

import Es6Promise from 'es6-promise'

require('es6-promise').polyfill();
Es6Promise.polyfill();

//vue全局过滤器
import './assets/js/filter'

import API from './application/api'
import ajax from './application/ajax'

//工具类函数
import Util from './assets/js/util'

//JS内置类原型上扩展方法(Object不可以)
import extendsFn from './assets/js/extends'
extendsFn();

Vue.prototype.$api = API;
Vue.prototype.$util = new Util;
Vue.prototype.$ajax = ajax
Vue.prototype.$Antv = Antv

//注入插件
Vue.use(Antd)
    .use(vChart)
    .use(RouterTab)
    .use(MyComponents);

window.vm = new Vue({
    router,
    store,
    render: h => h(App),
    created() {
        //调试
        //做缓存
        // window.console.dir(this.$http);
        // window.console.dir(this.$util);
        // window.console.dir(this.$wx);
        // window.console.dir(this.$store);
    },
    mounted() {
        // window.console.log(window.vm);
    }
}).$mount('#app');


import axios from 'axios'
import router from '@/application/router'
import {notification} from 'ant-design-vue'

let baseURL = '';
// if (process.env.NODE_ENV === 'development') {
//     if (process.env.VUE_APP_BUILD === 'devtest') {
//         //本地测试接口
//         // baseURL = 'http://39.105.17.185';
//     } else if(process.env.VUE_APP_BUILD === 'devall') {
//         //本地接口
//         baseURL = 'http://localhost:90';
//         // baseURL = 'http://39.105.17.185';
//     } else {
//         // baseURL = 'http://39.105.17.185';

//     }
// } else {
//     if (process.env.VUE_APP_BUILD === 'testing') {
//         //线上测试
//         // baseURL = 'http://39.105.17.185';
//     } else {
//         //线上生产
//         // baseURL = 'http://139.224.114.61:80';
//     }
// }

const ajax = (options) => {
    let option = {
        // 请求的接口，在请求的时候，如axios.get(url,config);这里的url会覆盖掉config中的url
        url: '',
        // 请求方法同上
        method: 'post', // default
        // 基础url前缀
        baseURL: baseURL,
        // 请求之前的数据
        // transformRequest: [function (data) {
        // 	// 这里可以在发送请求之前对请求数据做处理，比如form-data格式化等，这里可以使用开头引入的Qs（这个模块在安装axios的时候就已经安装了，不需要另外安装）
        // 	// data = Qs.stringify({});
        // 	return data
        // }],
        // // 请求之后的数据
        // transformResponse: [function (data) {
        //     // 这里提前处理返回的数据
        //     return data
        // }],
        // 请求头信息
        headers: {},
        // parameter参数
        params: {},
        // post参数，使用axios.post(url,{},config);如果没有额外的也必须要用一个空对象，否则会报错
        data: {},
        // 设置超时时间(毫秒)
        // timeout: 5000,
        timeout: 10 * 1000,
        // 返回数据类型
        responseType: 'json' // default
    }


    if (options !== null && options !== undefined && options instanceof Object) {

        let mlfastsessionid = sessionStorage.getItem('mlfastsessionid');
        if (mlfastsessionid) {
            option.headers['mlfastsessionid'] = mlfastsessionid
        }

        // 判断请求类型post情况需要JSON.stringify转码给后台
        if (options.method && options.method.toLowerCase() === 'get') {
            options.params = options.data
        } else {
            // 如果是正常类型请求, 将进行转码处理, 通常FormData类型为文件上传类型
            // if (!(options.data instanceof FormData)) {
            //     options.data = JSON.stringify(options.data)
            // }
        }

		/**
		 * 合并用户参数
		 * Object.assign(target, source1, source2)
		 */
        Object.assign(option, options)

        // 执行请求
        axios(option).then(res => {
            // 判断是否通讯正常
            if (res.status === 200) {
                // resultData 为 axios 返回结果集,需要处理后才能返回
                let rsd = res.data
                // 平台接口返回
                if (rsd) {
                    if (rsd.status !== undefined) {
                        if (rsd.status === 0) {//成功
                            let rs = rsd
                            if (option.success && typeof (option.success) === 'function') {
                                return option.success(rs.data);
                            }
                        } else if (rsd.code === 999) {//未登录
                            // Notify({ type: 'warning', message: rsd.msg });
                            notification.warning({ message: rsd.msg });
                            router.push({ path: '/Login', query: { redirect: router.currentRoute.fullPath } });
                        } else if (rsd.code === 998) {//接口无权限
                            // Notify({ type: 'warning', message: rsd.msg });
                            notification.warning({ message: rsd.msg });
                            return
                        } else {
                            // Toast({
                            //     type: 'fail',
                            //     message: rsd.message,
                            //     position: 'bottom',
                            //     closeOnClick: true// 点击后关闭
                            // });
                            notification.warning({ message: rsd.msg });
                            if (option.fail && typeof (option.fail) === 'function') {
                                return option.fail(rsd)
                            }
                        }
                    } else {
                        notification.error({ message: '【结果异常】', description: rsd.msg });
                        // Notify({ type: 'warning', message: '【结果异常】' });
                        // Toast.fail(`${rsd}`);
                    }
                } else {
                    // 如果是上传文件,包装返回值,其他场景暂时忽略
                    if ((option.data instanceof FormData)) {
                        let simdata = { 'code': 0, data: res, 'msg': 'FormData格式数据请求成功' }
                        if (option.success && typeof (option.success) === 'function') {
                            return option.success(simdata)
                        }
                    }
                    notification.error({ message: `[接口通讯正常,但未获取到异步请求 data 数据]${option}` });
                    // Toast.fail(`[接口通讯正常,但未获取到异步请求 data 数据]${option}`);
                }
            } else {
                // 返回状态非200情况
                // Notify({ type: 'danger', message: res });
                notification.error({ message: res });
            }
        }).catch(function (error) {
            notification.error({ message: `${error.message}` });
            // Toast(`${error.message}`);
            if (error.code === 'ECONNABORTED') {
                // Notify({ type: 'warning', message: '请求超时' });
            } else {
                // Notify({ type: 'danger', message: error.message });
            }
        })
    } else {
        notification.error({ message: '[数据异常]ajax参数为空' });
        // Notify({ type: 'danger', message: '[数据异常]ajax参数为空' });
    }
}

export default ajax

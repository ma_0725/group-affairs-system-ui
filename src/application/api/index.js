export default {

    /**
     * 公共模块
     */
    common: {
        login: '/api/admin/login/login',
        logout: '/api/admin/login/logout',
        upload: '/api/common/upload',
    },

    system: {
        user: {
            addUserRole: '/api/admin/system/addUserRole',
            deleteUserRole: '/api/admin/system/deleteUserRole',
            findRoleListByUserId: '/api/admin/system/findRoleListByUserId',
            findUserPage: '/api/admin/system/findUserPage',
            addUser: '/api/admin/system/addUser',
            updateUser: '/api/admin/system/updateUser',
            deleteUser: '/api/admin/system/deleteUsers',
        },
        role: {
            findAllMenuList: '/api/admin/system/findAllMenuList',
            addRoleMenu: '/api/admin/system/addRoleMenu',
            deleteRoleMenu: '/api/admin/system/deleteRoleMenu',
            findMenuListByRoleId: '/api/admin/system/findMenuListByRoleId',
            findRoleList: '/api/admin/system/findRoleList',
            findRolePage: '/api/admin/system/findRolePage',
            addRole: '/api/admin/system/addRole',
            updateRole: '/api/admin/system/updateRole',
            deleteRole: '/api/admin/system/deleteRole',
        },
        menu: {
            addMenuApi: '/api/admin/system/addMenuApi',
            deleteMenuApi: '/api/admin/system/deleteMenuApi',
            findApiListByMenuId: '/api/admin/system/findApiListByMenuId',
            findMenuList: '/api/admin/system/findMenuList',
            findMenuPage: '/api/admin/system/findMenuPage',
            addMenu: '/api/admin/system/addMenu',
            updateMenu: '/api/admin/system/updateMenu',
            deleteMenu: '/api/admin/system/deleteMenu',
        },
    },

    member: {
        saveOrUpdate: '/api/admin/member/saveOrUpdate',
        deleteByIds: '/api/admin/member/deleteByIds',
        findPage: '/api/admin/member/findPage',
        findList: '/api/admin/member/findList',
    },
    // 干部
    cadre: {
        saveOrUpdate: '/api/admin/cadre/saveOrUpdate',
        deleteByIds: '/api/admin/cadre/deleteByIds',
        findPage: '/api/admin/cadre/findPage',
        findList: '/api/admin/cadre/findList',
    },
    //奖惩
    rewardPunishment: {
        saveOrUpdate: '/api/admin/rewardPunishment/saveOrUpdate',
        deleteByIds: '/api/admin/rewardPunishment/deleteByIds',
        findPage: '/api/admin/rewardPunishment/findPage',
    },
    //推优
    excellence: {
        saveOrUpdate: '/api/admin/excellence/saveOrUpdate',
        deleteByIds: '/api/admin/excellence/deleteByIds',
        findPage: '/api/admin/excellence/findPage',
        findList: '/api/admin/excellence/findList',
    },
    //团费cost
    cost: {
        saveOrUpdate: '/api/admin/cost/saveOrUpdate',
        deleteByIds: '/api/admin/cost/deleteByIds',
        findPage: '/api/admin/cost/findPage',
        findList: '/api/admin/cost/findList',
    },
    //公告
    news: {
        saveOrUpdate: '/api/admin/news/saveOrUpdate',
        deleteByIds: '/api/admin/news/deleteByIds',
        findPage: '/api/admin/news/findPage',
        noReadList: '/api/admin/news/noReadList',
        logSave: '/api/admin/userNewsLog/saveOrUpdate',
    },
    //组织机构
    organization: {
        saveOrUpdate: '/api/admin/organization/saveOrUpdate',
        deleteByIds: '/api/admin/organization/deleteByIds',
        findPage: '/api/admin/organization/findPage',
        findList: '/api/admin/organization/tree',
    },
    //组织机构新
    relation: {
        saveOrUpdate: '/api/admin/relation/saveOrUpdate',
        deleteByIds: '/api/admin/relation/deleteByIds',
        findPage: '/api/admin/relation/findPage',
        findList: '/api/admin/relation/tree',
    },
    //选举
    election: {
        saveOrUpdate: '/api/admin/election/saveOrUpdate',
        deleteByIds: '/api/admin/election/deleteByIds',
        findPage: '/api/admin/election/findPage',
        findList: '/api/admin/election/findList',
    },

}
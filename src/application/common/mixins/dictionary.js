import ajax from '@/application/ajax'
import api from '@/application/api'
import dictionary from '../../../components/dictionary';
export default {
  data(){
    return{
      dictionaryList: []
    }
  },
  methods: {
    getDictionaryList(code){
      if (!code) {
        return false;
      }
      ajax({
        url: api.system.dictionary.findDictionaryList,
        data: {
          code: code
        },
        success: (result) => {
          if (result && result.length) {
            this.dictionaryList = result;
          }
        }
      })
    }
  }
}
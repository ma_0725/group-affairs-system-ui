import Vue from 'vue'
import VueRouter from 'vue-router'
import {
  notification
} from 'ant-design-vue'
Vue.use(VueRouter);

const Model = resolve => require(['@/pages/Model'], resolve);


const PersonCenter = resolve => require(['@/pages/person/Center'], resolve);

/**
 * 首页
 */
const HomeModel = resolve => require(['@/pages/home/Model'], resolve);
const HomeIndex = resolve => require(['@/pages/home/Index'], resolve);

/**
 * 系统设置
 */
const SystemModel = resolve => require(['@/pages/system/Model'], resolve);
const SystemMenu = resolve => require(['@/pages/system/menu/List'], resolve);
const SystemUser = resolve => require(['@/pages/system/user/List'], resolve);
const SystemRole = resolve => require(['@/pages/system/role/List'], resolve);
const SystemApi = resolve => require(['@/pages/system/api/List'], resolve);
const SystemDictionary = resolve => require(['@/pages/system/dictionary/List'], resolve);

/**
 * 设备
 */
const MemberModel = resolve => require(['@/pages/member/Model'], resolve);
const MemberList = resolve => require(['@/pages/member/List'], resolve);
const MemberForm = resolve => require(['@/pages/member/Form'], resolve);

/**
 * 登录
 */
const Login = resolve => require(['@/pages/login/Login'], resolve);

/**
 * 测试
 */
const Test = resolve => require(['@/pages/Test'], resolve);

const routes = [{
    path: '/',
    redirect: '/login',
    component: Model,
    children: [
      {
        name: 'index',
        path: '/index',
        component: HomeIndex,
        meta: {
          title: '系统首页',
          tabClass: 'custom-tab',
        },
      },
      {
        name: 'person',
        path: '/person',
        component: PersonCenter,
        meta: {
          title: '个人中心',
          tabClass: 'custom-tab',
        },
      },
      {
        name: 'menu',
        path: '/system/menu',
        component: SystemMenu,
        meta: {
          title: '菜单',
          tabClass: 'custom-tab',
        },
      },
      {
        name: 'user',
        path: '/system/user',
        component: SystemUser,
        meta: {
          title: '用户管理',
          tabClass: 'custom-tab',
        },
      },
      {
        name: 'role',
        path: '/system/role',
        component: SystemRole,
        meta: {
          title: '职位管理',
          tabClass: 'custom-tab',
        },
      },
      {
        name: 'api',
        path: '/system/api',
        component: SystemApi,
        meta: {
          title: '接口'
        },
      },
      {
        name: 'list',
        path: '/member/list',
        component: MemberList,
        meta: {
          title: '团员列表',
          tabClass: 'custom-tab',
        },
      },
      {
        name: 'form',
        path: '/member/form',
        component: MemberForm,
        meta: {
          title: '团员编辑',
          tabClass: 'custom-tab',
        },
      },
      {
        name: 'auditList',
        path: '/member/auditList',
        component: () => import('@/pages/member/auditList'),
        meta: {
          title: '入团审批列表',
          tabClass: 'custom-tab',
        },
      },
      {
        name: 'list',
        path: '/cadre/list',
        component: () => import('@/pages/cadre/List'),
        meta: {
          title: '干部管理',
          tabClass: 'custom-tab',
        },
      },
      {
        name: 'relationship',
        path: '/relationship/list',
        component: () => import('@/pages/relationship/list'),
        meta: {
          title: '组织关系列表',
          tabClass: 'custom-tab',
        },
      },
      {
        name: 'rewardPunishment',
        path: '/rewardPunishment/audiManage',
        component: () => import('@/pages/rewardPunishment/audiManage'),
        meta: {
          title: '奖惩列表',
          tabClass: 'custom-tab',
        },
      },
      {
        name: 'audiManage',
        path: '/excellence/audiManage',
        component: () => import('@/pages/excellence/audiManage'),
        meta: {
          title: '团员推优列表',
          tabClass: 'custom-tab',
        },
      },
        {
          name: 'auditList',
          path: '/excellence/auditList',
          component: () => import('@/pages/excellence/auditList'),
          meta: {
            title: '团员推优审核',
            tabClass: 'custom-tab',
          }
      },
      {
        name: 'costList',
        path: '/cost/list',
        component: () => import('@/pages/cost/list'),
        meta: {
          title: '团费缴费列表',
          tabClass: 'custom-tab',
        }
    },
    {
      name: 'costAudiList',
      path: '/cost/audiList',
      component: () => import('@/pages/cost/audiList'),
      meta: {
        title: '团费使用列表',
        tabClass: 'custom-tab',
      }
    },
    {
      name: 'organization',
      path: '/organization/list',
      component: () => import('@/pages/organization/list'),
      meta: {
        title: '机构列表',
        tabClass: 'custom-tab',
      }
    },
      {
        name: 'newsList',
        path: '/news/list',
        component: () => import('@/pages/news/list'),
        meta: {
          title: '公告列表',
          tabClass: 'custom-tab',
        }
    },
    {
      name: 'electionList',
      path: '/election/list',
      component: () => import('@/pages/election/list'),
      meta: {
        title: '选举列表',
        tabClass: 'custom-tab',
      }
  },
  {
    name: 'electionAuthList',
    path: '/election/authList',
    component: () => import('@/pages/election/authList'),
    meta: {
      title: '选举审核',
      tabClass: 'custom-tab',
    }
}
    ]
  },


  {
    name: 'login',
    path: '/login',
    component: Login,
    meta: {
      title: '登录'
    },
  },
  {
    name: 'test',
    path: '/test',
    component: Test,
    meta: {
      title: '登录'
    },
  },


];

// 获取原型对象上的push函数
const originalPush = VueRouter.prototype.push
// 编辑原型对象中的push方法
VueRouter.prototype.push = function push(location) {
  try {
    return originalPush.call(this, location).catch(err => err)
  } catch (error) {
    // console.log(error);
  }
}

const router = new VueRouter({
  routes,
  // mode: 'hash',
  // scrollBehavior() {
  //     return { x: 0, y: 0 }
  // },
  // linkExactActiveClass: 'my_active'
});

let menuList = [];
//跳转前拦截
router.beforeEach((to, from, next) => {
  let isAuth = false;
  if (!menuList || !menuList.length) {
    try {
      menuList = JSON.parse(sessionStorage.getItem('menuList'))
    } catch (e) {
      console.log(e);
    }
  }
  if (to.path == `/index` || to.path == `/` || to.path == `/login` || to.path == `/person`) {
    next();
  } else if (menuList && menuList.length) {
    //设置title
    if (to.meta.title) {
      document.title = to.meta.title;
    }
    menuList.map(menu => {
      if (menu.childrenList && menu.childrenList.length) {
        menu.childrenList.map(item => {
          if (`${item.path}` == to.path) {
            next();
            isAuth = true;
            return false;
          }
        })
      }
    })
    if (!isAuth) {
      notification.warning({
        message: '没有权限'
      });
    }
  }
});

//跳转后拦截
router.afterEach(() => {

});
export default router;
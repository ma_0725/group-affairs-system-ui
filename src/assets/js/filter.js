import Vue from 'vue'
;(() => {
 /**
     * 将时间戳转为目标时间格式
     */
    Vue.filter("showDate",(d,f) => {
        if(!d){
            return;
        }
        if(!f){
            f='yyyy-MM-dd HH:mm:ss';
        }
        var sd=new Date(d).pattern(f);
        return sd;
    });
    /**
     * 设置字段截取，默认截取长度30
     */
    Vue.filter('substr',(v,s) => {
        s=s||30;
        return v.substring(0,s);
    });
    /**
     * 设置字段默认值
     */
    Vue.filter('def',(v, s) =>{
        return v?v:s;
    });

    /**
     * 秒数转成时分秒格式
     * secondsToTime(100); // 01:40
     * secondsToTime(10000); // 02:46:40
     */
    Vue.filter('setSecondsToTime', v=> {
        if(v) {
            let hours = Math.floor(v / 3600),
                minutes = Math.floor(v % 3600 / 60),
                seconds = Math.ceil(v % 3600 % 60);
            hours = hours == 0 ? '' : hours < 10 ? '0' + hours + ':' : hours + ':';
            minutes = minutes < 10 ? '0' + minutes : minutes;
            seconds = seconds < 10 ? '0' + seconds : seconds;

            return hours + minutes + ':' + seconds;
        }
        return  '';
    });
})();

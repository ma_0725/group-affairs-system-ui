
function extendsObject() {

    /**
     * 数组去重
     */
    Array.prototype.myUnique = function () {
        return [... new Set(this)];
    };

    /**
     * 数组取最大值
     */
    Array.prototype.getMax = function() {
      return Math.max(...this);
    };

    /**
     * 数组取最小值
     */
    Array.prototype.getMin = function() {
        return Math.min(...this);
    };

    /**
     * 删除指定元素
     * 改变原来的数组,返回this
     */
    Array.prototype.remove = function(val) {
        let index = this.indexOf(val);
        while(index>-1){
            this.splice(index, 1);
            index = this.indexOf(val);
        }
        return this;
    };

    Date.prototype.format = function(fmt) {
        var o = {
          "M+": this.getMonth() + 1, //月份
          "d+": this.getDate(), //日
          "h+": this.getHours(), //小时
          "m+": this.getMinutes(), //分
          "s+": this.getSeconds(), //秒
          "q+": Math.floor((this.getMonth() + 3) / 3), //季度
          S: this.getMilliseconds() //毫秒
        };
        if (/(y+)/.test(fmt))
          fmt = fmt.replace(
            RegExp.$1,
            (this.getFullYear() + "").substr(4 - RegExp.$1.length)
          );
        for (var k in o)
          if (new RegExp("(" + k + ")").test(fmt))
            fmt = fmt.replace(
              RegExp.$1,
              RegExp.$1.length == 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length)
            );
        return fmt;
      };
}

export  default  extendsObject;

